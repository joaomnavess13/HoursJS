function carregar(){
    var msg = window.document.getElementById("msg");
    var img = window.document.getElementById("imagem");
    var data = new Date();
    var hora = data.getHours(); 
    

    msg.innerHTML = `Agora são ${hora} Horas`;

    if(hora >= 0 && hora < 12){
        //bom dia
        img.src = "imgs/dia.png";
        document.body.style.background = "#00a8ff";        
        
    }else if(hora >= 12 && hora <= 18  ){
        //boa tarde
        img.src = "imgs/tarde.png";
        document.body.style.background = "#e58e26";

    }else{
        //boa noite
        img.src= "imgs/noite.png";
        document.body.style.background = "#192a56";
    }    
}